// 1. Buat array of object students ada 5 data cukup
let cuteStudents = [{
        namePerson: 'Nala',
        address: 'DKI Jakarta',
        age: 24,
        status: 'Married',
    },
    {
        namePerson: 'Aksa',
        address: 'Jawa Timur',
        age: 19,
        status: 'Single',
    },
    {
        namePerson: 'Raja',
        address: 'Jawa Barat',
        age: 25,
        status: 'Married',
    },
    {
        namePerson: 'Nesya',
        address: 'Jawa Tengah',
        age: 20,
        status: 'Single',
    },
    {
        namePerson: 'Safira',
        address: 'DKI Jakarta',
        age: 21,
        status: 'Single',
    }
]

// 2. Buat 3 function, 1 function nentuin provinsi kalian ada di Jawa Barat atau enggak, umur kalian diatas 22 atau enggak, status kalian single atau enggak

// a. Function Menunjukkan Provinsi di Jawa Barat/Tidak
function cuteStudentsData(studentsData) {
    return studentsData;
}
// console.log(cuteStudentsData(cuteStudents));

function personAddress(studentsData) {
    let personAddress = cuteStudentsData(studentsData);
    let result = [];
    for (let i = 0; i < personAddress.length; i++) {
        if (personAddress[i].address == 'Jawa Barat') {
            result.push(`${personAddress[i].namePerson} live in ${personAddress[i].address}`);
            // console.log('This student live in ' + personAddress[i].address)
            // console.log(`${personAddress[i].namePerson} live in ${personAddress[i].address}`)
        } else {
            result.push(`${personAddress[i].namePerson} do not live in Jawa Barat, but live in ${personAddress[i].address}`);
        }
    }
    return result;
}
console.log(personAddress(cuteStudents));


// b. Function Menunjukkan Umur > 22/Tidak
function personAge(studentsData) {
    let personAge = cuteStudentsData(studentsData);
    let result = [];
    for (let i = 0; i < personAge.length; i++) {
        if (personAge[i].age > 22) {
            // console.log('This student age is ' + personAge[i].age)
            // console.log(`${personAge[i].namePerson} age is ${personAge[i].age}`)
            result.push(`${personAge[i].namePerson} age is ${personAge[i].age} which means older than 22`);
        } else {
            result.push(`${personAge[i].namePerson} age is not older than 22, but ${personAge[i].age}`);
        }
    }
    return result;
}
console.log(personAge(cuteStudents));


// c. Function Menunjukkan Status Single/Tidak
function personStatus(studentsData) {
    let personStatus = cuteStudentsData(studentsData);
    let result = [];
    for (let i = 0; i < personStatus.length; i++) {
        if (personStatus[i].status == 'Single') {
            // console.log(`${personStatus[i].namePerson} status is ${personStatus[i].status}`)
            result.push(`${personStatus[i].namePerson} status is ${personStatus[i].status}`);
        } else {
            result.push(`${personStatus[i].namePerson} status is not Single, but ${personStatus[i].status}`);
        }
    }
    return result;
}
console.log(personStatus(cuteStudents));


// 3. Callback function untuk print hasil proses 3 function diatas. 
// Contoh: nama saya imam, saya tinggal di Jawa Barat, umur saya dibawah 22 tahun. dan status saya single loh. CODE buat Nilam

function forEach(arrayStudents, callback) {
    const result = [];
    for (let i = 0; i < arrayStudents.length; i++) {
        if (arrayStudents[i].address == 'Jawa Barat' && arrayStudents[i].age > 22) {
            // console.log(array[i])
            result.push(callback(arrayStudents[i]));
        }
    }
    return result;
}
const studentsCallback = forEach(cuteStudents, (item) => {
    console.log(`My name is ${item.namePerson}, I live in ${item.address}, I am ${item.age} years old, my status is ${item.status}`)
    return item;
});
// console.log(studentsCallback);
