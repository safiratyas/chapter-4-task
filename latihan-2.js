// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';
class Vehicle {
    constructor(type, country) {
        this.type = type;
        this.country = country;
    }

    info() {
        console.log(`This type of car is ${this.type}, made by ${this.country}`);
    }

}

// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak

class Car extends Vehicle {
    constructor(type, country, brand, price, tax) {
        super(type, country);
        this.brand = brand;
        this.price = price;
        this.tax = tax;
    }

    // 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
    totalPrice() {
        let sales = this.price + ((this.tax/100) * this.price)
        return sales;
    }

    // 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
    // print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'
    info() {
        super.info();
        console.log(`This type of car is ${this.type}, from a brand ${this.brand}, the vehicle price is Rp.${this.totalPrice()}`)
    }
}


// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 
// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()

// Initiate from Vehicle
let vehicleTwo = new Vehicle('Two Wheels', 'Italy');
vehicleTwo.info();

// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

// Initiate from Car
let carOne = new Car('Four Wheels', 'America', 'Tesla', 800000000, 10);
carOne.info();

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/


// 1. Lat