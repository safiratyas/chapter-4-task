const fs = require("fs")

const createPerson = function (person) {
    fs.writeFileSync('./person.json', JSON.stringify(person))
    return person;
}

const Safira = createPerson({
    name: 'Safira Tyas Wandita',
    age: 22,
    address: 'Jaksel Pride'
})