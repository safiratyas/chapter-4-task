// import module fs 
const fs = require('fs')

// read file with fs
const isi = fs.readFileSync('./text.txt','utf-8')
console.log(isi)

// write file with fs
fs.writeFileSync('./test.txt', "I love myself")

// items from person
const items = require('./person.json')

// import module express
const express = require('express');
const app = express();

// app.get('/', (req, res) => {
//     res.render('index.ejs')
// })

app.get('/', (req, res) => {
    res.render('item.ejs', {
        data: items
    })
})

app.listen(3000);

